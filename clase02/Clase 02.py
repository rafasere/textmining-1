"""
# Text Mining Clase 02 
"""

"""
Codigo fuente de este notebook ha sido extraido de:<br>
* Deep Learning for Natural Language Processing    
* Medium Word2Vec Model
* Kavita Ganesan - Gensim Word2Vec
"""

"""
Es el paquete más común para trabajar con corpus, categorizando texto, analizando la estructura lingüística y moras.
"""
import nltk
sent_ = "I am almost dead this time"
tokens_ = nltk.word_tokenize(sent_)
tokens_


"""
#### Synonyms
"""
from nltk.corpus import wordnet
word_ = wordnet.synsets("spectacular")
word_

word_[1].definition()
word_[2].definition()
word_[3].definition()


"""
#### Stemming
"""
from nltk.stem import PorterStemmer
stemmer = PorterStemmer()
stemmer.stem("decreases")

"""
#### Lemmatization
"""
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
lemmatizer.lemmatize("decreases")

"""
#### Part of speech
"""
text = nltk.word_tokenize("And now for something completely different")
nltk.pos_tag(text)

"""
#### Load Corpus
"""
mywords = nltk.data.load('./data/mywords.txt', format='raw')
mywords

"""
____________________________
"""

# %%
"""
# TextBlob
"""

# %%
from textblob import TextBlob

# %%
statement = TextBlob("My home is far away from my school.")

# %%
statement.sentiment

# %%
text = '''How about you and I go together on a walk far away
from this place, discussing the things we have never discussed
on Deep Learning and Natural Language Processing.'''

# %%
blob_ = TextBlob(text)

# %%
blob_

# %%
blob_.tags

# %%
sample_ = TextBlob("I thinkk the model needs to be trained more!")

# %%
sample_.correct()

# %%
lang_ = TextBlob(u"Voulez-vous apprendre le français?")

# %%
lang_.translate(from_lang='fr', to='en')

# %%
"""
_______________________________________
"""

# %%
"""
# SpaCy
"""

# %%
"""
### Named Entity Recognition
"""

# %%
import spacy

# %%
nlp = spacy.load("en")

# %%
william_wikidef = """William was the son of King William
II and Anna Pavlovna of Russia. On the abdication of his
grandfather William I in 1840, he became the Prince of Orange.
On the death of his father in 1849, he succeeded as king of the
Netherlands. William married his cousin Sophie of Württemberg
in 1839 and they had three sons, William, Maurice, and
Alexander, all of whom predeceased him. """

# %%
nlp_william = nlp(william_wikidef)

# %%
[ (i, i.label_, i.label) for i in nlp_william.ents]

# %%
senten_ = nlp('The book deals with NLP')

# %%
for noun_ in senten_.noun_chunks:
    print(noun_)
    print(noun_.text)
    print(noun_.root.dep_)
    print(noun_.root.head.text)

# %%
"""
_______________________
"""

# %%
"""
# Gensim
"""

# %%
"""
Se usa principalmente para modelar temas y similitud de documentos.<br>
Es utilizada para obtener un vector de palabras.
"""

# %%
from gensim.models import Word2Vec

# %%
import pandas as pd

# %%
"""
La idea detrás de ***Word2Vec*** es bastante simple.<br> 
Se asume que el significado de una palabra puede inferirse por la compañía que mantiene.<br>
Esto es análogo al dicho: ***"muéstrame a tus amigos y te diré quién eres"***.<br>
"""

# %%
"""
Si tiene dos palabras que tienen vecinos muy similares (es decir, el contexto en el que se usa es casi el mismo), 
entonces estas palabras probablemente tengan un significado bastante similar o al menos estén relacionadas.<br>
Por ejemplo, las palabras conmocionado, **horrorizado** y **asombrado** se usan generalmente en un contexto similar.<br>
"""

# %%
min_count = 0
size = 50
window = 2

# %%
sentences= "bitcoin is an innovative payment network and a new kind of money."

# %%
sentences=sentences.split()

# %%
sentences

# %%
model = Word2Vec(sentences, min_count=min_count, size=size, window=window)

# %%
model['b']

# %%
df = pd.read_csv('./data/data.csv')

# %%
df.head()

# %%
df['Maker_Model']= df['Make']+ " " + df['Model']

# %%
df1 = df[['Engine Fuel Type','Transmission Type','Driven_Wheels','Market Category','Vehicle Size', 'Vehicle Style', 'Maker_Model']]

# %%
df1.head()

# %%
df2 = df1.apply(lambda x: ','.join(x.astype(str)), axis=1)

# %%
df2.head()

# %%
df_clean = pd.DataFrame({'clean': df2})

# %%
df_clean.head()

# %%
sent = [row.split(',') for row in df_clean['clean']]

# %%
sent[:2]

# %%
model = Word2Vec(sent, size= 50, window =3, min_count=1, workers=3, sg = 1)

# %%
"""
**size**: The number of dimensions of the embeddings and the default is 100.<br>
**window**: The maximum distance between a target word and words around the target word. The default window is 5.<br>
**min_count**: The minimum count of words to consider when training the model; words with occurrence less than this count will be ignored. The default for min_count is 5.<br>
**workers**: The number of partitions during training and the default workers is 3.<br>
**sg**: The training algorithm, either CBOW(0) or skip gram(1). The default training algorithm is CBOW.<br>
"""

# %%
model.wv['Toyota Camry']

# %%
model.wv.similarity('Porsche 718 Cayman', 'Nissan Van')

# %%
df1[df1['Maker_Model'] == 'Porsche 718 Cayman']

# %%
df1[df1['Maker_Model'] == 'Nissan Van']

# %%
model.wv.similarity('Porsche 718 Cayman', 'Mercedes-Benz SLK-Class')

# %%
df1[df1['Maker_Model'] == 'Mercedes-Benz SLK-Class']

# %%
model.wv.most_similar('Mercedes-Benz SLK-Class')[:5]

# %%


# %%
"""
______________________
"""

# %%
"""
# Vectorization
"""

# %%
"""
La vectorización es una herramienta de biblioteca SciKit-Learn que toma cualquier masa de texto y devuelve cada palabra única como una característica, con un recuento del número
de veces aparece una palabra en particular en el texto.
"""

# %%
from sklearn.feature_extraction.text import CountVectorizer

# %%
texts=["Ramiess sings classic songs",
       "he listens to old pop ", 
       "and rock music", 
       "and also listens to classical songs"]

# %%
cv = CountVectorizer()

# %%
cv_fit=cv.fit_transform(texts)

# %%
cv.get_feature_names()

# %%
cv_fit.toarray()

# %%
"""
______________________
"""

# %%
"""
# TF-IDF (Term-Frequency Inverse Document-Frequency)   
"""

# %%
"""
En corpus de texto grande, algunas palabras estarán muy presentes (por ejemplo, "the", "a", "is" en inglés),
por lo tanto, llevarán muy poca información significativa sobre el contenido real del documento. 
Si tuviéramos se haria un conteo directo, esos términos se posicionarian sobre los términos más interesantes.
"""

# %%
"""
Para volver a ponderar las características de conteo en valores de coma flotante adecuados para el uso de un clasificador, es muy común usar la transformación tf - idf.
"""

# %%
"""
Supongamos que un documento contiene 100 palabras, en donde la palabra ***feliz*** aparece 5 veces.<br> 
El término frecuencia (***TF***) para feliz es entonces (5/100) = **0.05**.<br>
***IDF***, por otro lado, es una relación de registro del número total de documentos a un documento que contiene una palabra en particular.<br> 
Supongamos que tenemos 10 millones de documentos, y la palabra ***feliz*** aparece en 1,000 veces.<br>
La frecuencia inversa del documento (***IDF***), se calcularía como log (10,000,000 / 1,000) = **4**.<br>
Por lo tanto, el ***TF-IDF*** es el producto de estas cantidades: 0.05 × 4 =**0.20**.<br>
"""

# %%
from sklearn.feature_extraction.text import TfidfVectorizer

# %%
texts=["Ramiess sings classic songs",
       "he listens to old pop",
       "and rock music", 
       "and also listens to classical songs"]

# %%
vect = TfidfVectorizer()

# %%
X = vect.fit_transform(texts)

# %%
vect.get_feature_names() 

# %%
X.toarray()

# %%
X.toarray()[0]

# %%
X.toarray()[1]

# %%
"""
_______________________________
"""

# %%
"""
# Text Classifier
"""

# %%
from textblob import TextBlob

# %%
from textblob.classifiers import NaiveBayesClassifier

# %%
data = [
('I love my country.', 'pos'),
('This is an amazing place!', 'pos'),
('I do not like the smell of this place.', 'neg'),
('I do not like this restaurant', 'neg'),
('I am tired of hearing your nonsense.', 'neg'),
("I always aspire to be like him", 'pos'),
("It's a horrible performance.", "neg")
]

# %%
model = NaiveBayesClassifier(data)

# %%
model.classify("It's an awesome place!")

# %%
"""
___________________________
"""

# %%
from classification import precision_recall, MaxVoteClassifier

# %%
from featx import label_feats_from_corpus, split_label_feats, high_information_words, bag_of_words_in_set

# %%
from nltk.corpus import movie_reviews
from nltk.classify.util import accuracy
from nltk.classify import NaiveBayesClassifier
from nltk.classify import MaxentClassifier
from nltk.classify import DecisionTreeClassifier
from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.svm import LinearSVC

# %%
labels = movie_reviews.categories()

# %%
labeled_words = [(l, movie_reviews.words(categories=[l])) for l in labels]

# %%
high_info_words = set(high_information_words(labeled_words))

# %%
feat_det = lambda words: bag_of_words_in_set(words, high_info_words)

# %%
lfeats = label_feats_from_corpus(movie_reviews, feature_detector=feat_det)

# %%
train_feats, test_feats = split_label_feats(lfeats)

# %%
"""
[Precisión y exhaustividad](https://developers.google.com/machine-learning/crash-course/classification/precision-and-recall)
"""

# %%
"""
[Exactitud](https://developers.google.com/machine-learning/crash-course/classification/accuracy)
"""

# %%
"""
[ROC y AUC](https://developers.google.com/machine-learning/crash-course/classification/roc-and-auc)
"""

# %%


# %%
nb_classifier = NaiveBayesClassifier.train(train_feats)

# %%
"Accuracy Naive Bayes: " + str(accuracy(nb_classifier, test_feats))

# %%
nb_precisions, nb_recalls = precision_recall(nb_classifier, test_feats)

# %%
"Precisions Naive Bayes Pos: " + str(nb_precisions['pos'])

# %%
"Precisions Naive Bayes Neg: " + str(nb_precisions['neg'])

# %%
"Recalls Naive Bayes Pos: " + str(nb_recalls['pos'])

# %%
"Recalls Naive Bayes Neg: " + str(nb_recalls['neg'])

# %%
me_classifier = MaxentClassifier.train(train_feats, algorithm='gis', trace=0, max_iter=10, min_lldelta=0.5)

# %%
"Accuracy Max Entropy: " + str(accuracy(me_classifier, test_feats))

# %%
me_precisions, me_recalls = precision_recall(me_classifier, test_feats)

# %%
"Precisions Max Entropy Pos: " + str(me_precisions['pos'])

# %%
"Precisions Max Entropy Neg: " + str(me_precisions['neg'])

# %%
"Recalls Max Entropy Pos: " + str(me_recalls['pos'])

# %%
"Recalls Max Entropy Neg: " + str(me_recalls['neg'])

# %%
dt_classifier = DecisionTreeClassifier.train(train_feats, binary=True, depth_cutoff=20, support_cutoff=20, entropy_cutoff=0.01)

# %%
"Accuracy Decision Tree: " + str(accuracy(dt_classifier, test_feats))

# %%
dt_precisions, dt_recalls = precision_recall(dt_classifier, test_feats)

# %%
"Precisions Decision Tree Pos: " + str(dt_precisions['pos'])

# %%
"Precisions Decision Tree Neg: " + str(dt_precisions['neg'])

# %%
"Recalls Decision Tree Pos: " + str(dt_recalls['pos'])

# %%
"Recalls Decision Tree Neg: " + str(dt_recalls['neg'])

# %%
sk_classifier = SklearnClassifier(LinearSVC()).train(train_feats)

# %%
"Accuracy Sklearn Linear SVC: " + str(accuracy(sk_classifier, test_feats))

# %%
sk_precisions, sk_recalls = precision_recall(sk_classifier, test_feats)

# %%
"Precisions Sklearn Linear SVC Pos: " + str(sk_precisions['pos'])

# %%
"Precisions Sklearn Linear SVC Neg: " + str(sk_precisions['neg'])

# %%
"Recalls Sklearn Linear SVC Pos: " + str(sk_recalls['pos'])

# %%
"Recalls Sklearn Linear SVC Neg: " + str(sk_recalls['neg'])

# %%
mv_classifier = MaxVoteClassifier(nb_classifier, dt_classifier, me_classifier, sk_classifier)

# %%
"Accuracy Max Vote: " + str(accuracy(mv_classifier, test_feats))

# %%
mv_precisions, mv_recalls = precision_recall(mv_classifier, test_feats)

# %%
"Precisions Max Vote Pos: " + str(mv_precisions['pos'])

# %%
"Precisions Max Vote Neg: " + str(mv_precisions['neg'])

# %%
"Recalls Max Vote Pos: " + str(mv_recalls['pos'])

# %%
"Recalls Max Vote Neg: " + str(mv_recalls['neg'])

# %%


# %%


# %%


# %%


# %%


# %%
